# Wstęp #

Celem zadania jest napisanie aplikacji konsolowej przyjmującej STDIN w formie tekstowej i
zwracającej rezultat do STDOUT. Przeprowadzamy import początkowego stanu magazynu.
Każdy materiał ma określoną nazwę i identyfikator oraz znajduje się w różnych ilościach w
jednym lub kilku magazynach.

# Założenia #
• ignorujemy wiersze zaczynające się od znaku #
• każdy wiersz (poza ignorowanymi) jest zapisany w następującym formacie:
Nazwa materiału;ID Materiału;Magazyn,Ilość|Magazyn,Ilość|Magazyn,Ilość
• każdy wiersz (poza ignorowanymi) jest kompletny i poprawnie sformatowany
• każdy wymieniony materiał znajduje się przynajmniej w jednym magazynie, ale ich ilość
dla poszczególnych materiałów może być różna

# Oczekiwany format rozwiązania #
• magazyny powinny zostać pogrupowane i posortowane malejąco względem sumy ilości
materiałów w nich zawartych, format:
[nazwa magazynu] (total [ilość materiałów])
• magazyny powinny zostać oddzielone od siebie pustą linią
• jeśli istnieją magazyny z taką samą ilością materiałów, należy posortować je malejąco
alfabetycznie (Z-A)
• materiały w obrębie każdego magazynu powinny zostać posortowane rosnąco
alfabetycznie (A-Z) według ID i wyświetlone w osobnych liniach w formacie:
[id materiału]: ilość w magazynie

# How to run #

Run command in command line

cmd> warehause.exe path-to-input-file