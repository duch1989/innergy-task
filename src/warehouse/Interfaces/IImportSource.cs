﻿namespace warehouse.Interfaces
{
    public interface IImportDataSource
    {
        string GetNext();
    }
}
