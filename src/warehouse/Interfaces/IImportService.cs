﻿namespace warehouse.Interfaces
{
    public interface IImportService
    {
        void Import(IImportDataSource importDataSource);
    }
}
