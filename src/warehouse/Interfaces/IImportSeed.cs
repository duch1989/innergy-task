﻿using System;
using System.Collections.Generic;
using System.Text;

namespace warehouse.Interfaces
{
    interface IImportSeed
    {
        void Seed(string[] inputData);
    }
}
