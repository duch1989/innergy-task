﻿using System;
using System.Collections.Generic;
using System.Text;

namespace warehouse.Interfaces
{
    public interface IRepository<T>
    {
        void Add(T entity);

        IEnumerable<T> GetAll();

        void RemoveAll();
    }
}
