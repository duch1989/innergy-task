﻿using System;
using System.Collections.Generic;
using System.Linq;
using warehouse.Data;
using warehouse.Interfaces;

namespace warehouse.Repositories
{
    public class ProductRepository : IRepository<Product>
    {
        private static readonly IList<Product> _products = new List<Product>();

        public void Add(Product newEntity)
        {
            Validate(newEntity);

            _products.Add(newEntity);
        }

        public IEnumerable<Product> GetAll()
        {
            return _products;
        }

        public void RemoveAll()
        {
            _products.Clear();
        }

        private void Validate(Product newEntity)
        {
            if(newEntity == null)
            {
                throw new ArgumentNullException();
            }

            if (_products.Any(p => p.Id.Equals(newEntity.Id)))
            {
                throw new ArgumentException($"{nameof(Product)} already exist");
            }

            if (string.IsNullOrEmpty(newEntity.Id) || string.IsNullOrEmpty(newEntity.Name))
            {
                throw new ArgumentException($"{nameof(Product.Id)} or {nameof(Product.Name)} of new product is missing");
            }
        }
    }
}
