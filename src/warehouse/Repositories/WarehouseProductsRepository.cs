﻿using System;
using System.Collections.Generic;
using System.Linq;
using warehouse.Data;
using warehouse.Interfaces;

namespace warehouse.Repositories
{
    public class WarehouseProductsRepository : IRepository<WarehouseProduct>
    {
        private static readonly IList<WarehouseProduct> _warehouseProducts = new List<WarehouseProduct>();

        public void Add(WarehouseProduct entity)
        {
            Validate(entity);

            _warehouseProducts.Add(entity);
        }

        public IEnumerable<WarehouseProduct> GetAll()
        {
            return _warehouseProducts;
        }

        public void RemoveAll()
        {
            _warehouseProducts.Clear();
        }

        private void Validate(WarehouseProduct entity)
        {
            if(entity == null)
            {
                throw new ArgumentNullException();
            }

            if(_warehouseProducts.Any(p => p.ProductId.Equals(entity.ProductId) && p.WarehouseId.Equals(entity.WarehouseId)))
            {
                throw new ArgumentException("Entity already exists");
            }

            if (string.IsNullOrEmpty(entity.ProductId) || string.IsNullOrEmpty(entity.WarehouseId))
            {
                throw new ArgumentException($"{nameof(WarehouseProduct.ProductId)} or {nameof(WarehouseProduct.WarehouseId)} is missing");
            }

            if (entity.Quantity < 1)
            {
                throw new ArgumentException($"{nameof(WarehouseProduct.Quantity)} is less then 0");
            }
        }
    }
}
