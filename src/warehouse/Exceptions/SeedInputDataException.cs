﻿using System;
using System.Collections.Generic;
using System.Text;

namespace warehouse.Exceptions
{
    public class SeedInputDataException : Exception
    {
        public SeedInputDataException() : base("Wrong input data.")
        {

        }
    }
}
