﻿using System;
using System.Collections.Generic;
using System.Text;

namespace warehouse.Data
{
    public class WarehouseProduct
    {
        public string WarehouseId { get; set; }

        public string ProductId { get; set; }

        public int Quantity { get; set; }
    }
}
