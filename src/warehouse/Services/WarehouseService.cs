﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using warehouse.Data;
using warehouse.Dtos;
using warehouse.Interfaces;
using warehouse.Repositories;

namespace warehouse.Services
{
    public class WarehouseService
    {
        private IRepository<WarehouseProduct> _warehouseProductsRepository;

        public WarehouseService()
        {
            _warehouseProductsRepository = new WarehouseProductsRepository();
        }

        public IEnumerable<WarehouseDto> GetSummary()
        {
            return _warehouseProductsRepository.GetAll()
                .GroupBy(w => w.WarehouseId)
                .Select(w => new WarehouseDto
                {
                    Name = w.Key,
                    Products = w.Select(p => new WarehouseProductDto { Name = p.ProductId, Quantity = p.Quantity }).OrderBy(p => p.Name)
                })
                .GroupBy(r => r.TotalProducts)
                .Select(r => r.OrderByDescending(p => p.Name))
                .SelectMany(r => r)
                .ToList();
        }
    }
}
