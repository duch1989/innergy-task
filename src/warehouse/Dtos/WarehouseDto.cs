﻿using System.Collections.Generic;
using System.Linq;

namespace warehouse.Dtos
{
    public class WarehouseDto
    {
        public string Name { get; set; }

        public int TotalProducts
        {
            get
            {
                return Products?.Sum(p => p.Quantity) ?? 0; 
            }
        }

        public IEnumerable<WarehouseProductDto> Products { get; set; }
    }
}
