﻿using System;
using System.Collections.Generic;
using System.Text;

namespace warehouse.Dtos
{
    public class WarehouseProductDto
    {
        public string Name { get; set; }

        public int Quantity { get; set; }
    }
}
