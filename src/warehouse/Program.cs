﻿using System;
using System.IO;
using warehouse.Import;
using warehouse.Services;

namespace warehouse
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("Please provide file to process in args");
                Console.ReadLine();

                return;
            }

            if (!File.Exists(args[0]))
            {
                Console.WriteLine("File doesn't exist.");
                Console.ReadLine();

                return;
            }

            var importService = new ImportService();
            importService.Import(new FileImportSource(args[0]));

            var warehouseService = new WarehouseService();
            var warehouses = warehouseService.GetSummary();

            using (var file = new StreamWriter("result.txt"))
            {
                foreach (var warehouse in warehouses)
                {
                    file.WriteLine($"{warehouse.Name} (total {warehouse.TotalProducts})");

                    foreach(var product in warehouse.Products)
                    {
                        file.WriteLine($"{product.Name}: {product.Quantity}");
                    }

                    file.WriteLine(Environment.NewLine);
                }
            }

            Console.WriteLine("Done");
        }
    }
}
