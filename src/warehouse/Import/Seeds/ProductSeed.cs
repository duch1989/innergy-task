﻿using System;
using warehouse.Data;
using warehouse.Exceptions;
using warehouse.Interfaces;
using warehouse.Repositories;

namespace warehouse.Import.Seeds
{
    public class ProductSeed : IImportSeed
    {
        private const int IdFieldIndex = 1;
        private const int NameFieldIndex = 0;

        private IRepository<Product> _productRepository;

        public ProductSeed()
        {
            _productRepository = new ProductRepository();
        }

        public void Seed(string[] inputData)
        {
            if(inputData.Length < 2)
            {
                throw new SeedInputDataException();
            }

            var newProduct = new Product
            {
                Id = inputData[IdFieldIndex],
                Name = inputData[NameFieldIndex]
            };

            _productRepository.Add(newProduct);
        }
    }
}
