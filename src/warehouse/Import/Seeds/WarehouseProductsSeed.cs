﻿using System;
using warehouse.Data;
using warehouse.Exceptions;
using warehouse.Interfaces;
using warehouse.Repositories;

namespace warehouse.Import.Seeds
{
    public class WarehouseProductsSeed : IImportSeed
    {
        private const string WarehouseProductsSeparator = "|";
        private const string WarehouseProductSeparator = ",";

        private const int ProductIdFieldIndex = 1;
        private const int WarehouseIdFieldIndex = 0;
        private const int QuantityFieldIndex = 1;
        
        private IRepository<WarehouseProduct> _warehouseProductsRepository;

        public WarehouseProductsSeed()
        {
            _warehouseProductsRepository = new WarehouseProductsRepository();
        }

        public void Seed(string[] inputData)
        {
            if(inputData.Length < 3)
            {
                throw new SeedInputDataException();
            }

            var warehouseProducts = inputData[2].Split(WarehouseProductsSeparator);

            foreach(var warehouseProduct in warehouseProducts)
            {
                var warehouseProductArray = warehouseProduct.Split(WarehouseProductSeparator);

                var newWarehouseProduct = new WarehouseProduct
                {
                    ProductId = inputData[ProductIdFieldIndex],
                    WarehouseId = warehouseProductArray[WarehouseIdFieldIndex],
                    Quantity = int.Parse(warehouseProductArray[QuantityFieldIndex])
                };

                _warehouseProductsRepository.Add(newWarehouseProduct);
            }
        }
    }
}
