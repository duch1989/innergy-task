﻿using System.Collections.Generic;
using warehouse.Import.Seeds;
using warehouse.Interfaces;

namespace warehouse.Import
{
    public class ImportService : IImportService
    {
        private const string CommentSign = "#";
        private const string DataSeparator = ";";

        private IEnumerable<IImportSeed> ImportSeeds = new List<IImportSeed>
        { 
            new ProductSeed(),
            new WarehouseProductsSeed()
        };

        public void Import(IImportDataSource importDataSource)
        {
            var row = importDataSource.GetNext();

            while (row != null){
                if (row == string.Empty || row.StartsWith(CommentSign))
                {
                    row = importDataSource.GetNext();

                    continue;
                }

                var inputDataArray = row.Split(DataSeparator);
                
                foreach(var importSeed in ImportSeeds)
                {
                    importSeed.Seed(inputDataArray);
                }

                row = importDataSource.GetNext();
            }
        }
    }
}
