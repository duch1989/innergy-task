﻿using System;
using System.IO;
using warehouse.Interfaces;

namespace warehouse.Import
{
    public class FileImportSource : IImportDataSource, IDisposable
    {
        public readonly StreamReader _streamReader;

        public FileImportSource(string filePath)
        {
            if (!File.Exists(filePath))
            {
                throw new FileNotFoundException();
            }

            var fileStream = new FileStream(filePath, FileMode.Open);
            _streamReader = new StreamReader(fileStream);
        }

        public void Dispose()
        {
            _streamReader.Dispose();
        }

        public string GetNext()
        {
            return _streamReader.ReadLine();
        }
    }
}
