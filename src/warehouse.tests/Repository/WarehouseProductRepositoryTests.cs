﻿using NUnit.Framework;
using System;
using System.Linq;
using warehouse.Data;
using warehouse.Repositories;

namespace warehouse.tests.Repository
{
    public class WarehouseProductRepositoryTests
    {
        [TearDown]
        public void Cleanup()
        {
            var warehouseProductsRepository = new WarehouseProductsRepository();
            warehouseProductsRepository.RemoveAll();
        }

        [Test]
        public void AddNullValue()
        {
            var warehouseProductsRepository = new WarehouseProductsRepository();

            Assert.Throws<ArgumentNullException>(() => warehouseProductsRepository.Add(null));
        }

        [Test]
        public void AddInvalidEntityWithWrongWarehouseId()
        {
            var warehouseProductsRepository = new WarehouseProductsRepository();

            Assert.Throws<ArgumentException>(() => warehouseProductsRepository.Add(new WarehouseProduct
            {
                WarehouseId = string.Empty,
                ProductId = "test",
                Quantity = 1
            }));
        }

        [Test]
        public void AddInvalidEntityWithWrongProductId()
        {
            var warehouseProductsRepository = new WarehouseProductsRepository();

            Assert.Throws<ArgumentException>(() => warehouseProductsRepository.Add(new WarehouseProduct
            {
                WarehouseId = "test",
                ProductId = string.Empty,
                Quantity = 1
            }));
        }

        [Test]
        public void AddInvalidEntityWithWrongQuantity()
        {
            var warehouseProductsRepository = new WarehouseProductsRepository();

            Assert.Throws<ArgumentException>(() => warehouseProductsRepository.Add(new WarehouseProduct
            {
                WarehouseId = "test",
                ProductId = "test"
            }));
        }

        [Test]
        public void AddCorrectEntity()
        {
            var warehouseProductsRepository = new WarehouseProductsRepository();
            var newProduct = new WarehouseProduct
            {
                WarehouseId = "test",
                ProductId = "test",
                Quantity = 1
            };

            warehouseProductsRepository.Add(newProduct);

            Assert.IsTrue(warehouseProductsRepository.GetAll().Count() == 1);
            Assert.AreEqual(warehouseProductsRepository.GetAll().First().WarehouseId, newProduct.WarehouseId);
            Assert.AreEqual(warehouseProductsRepository.GetAll().First().ProductId, newProduct.ProductId);
            Assert.AreEqual(warehouseProductsRepository.GetAll().First().Quantity, newProduct.Quantity);
        }

        [Test]
        public void AddExistingEntityException()
        {
            var warehouseProductsRepository = new WarehouseProductsRepository();
            var newProduct = new WarehouseProduct
            {
                WarehouseId = "test",
                ProductId = "test",
                Quantity = 1
            };

            warehouseProductsRepository.Add(newProduct);

            Assert.Throws<ArgumentException>(() => warehouseProductsRepository.Add(newProduct));
        }

        [Test]
        public void RemoveEntity()
        {
            var warehouseProductsRepository = new WarehouseProductsRepository();
            var newProduct = new WarehouseProduct
            {
                WarehouseId = "test",
                ProductId = "test",
                Quantity = 1
            };

            warehouseProductsRepository.Add(newProduct);
            warehouseProductsRepository.RemoveAll();

            Assert.IsTrue(warehouseProductsRepository.GetAll().Count() == 0);
        }
    }
}
