﻿using NUnit.Framework;
using System;
using System.Linq;
using warehouse.Data;
using warehouse.Repositories;

namespace warehouse.tests.Repository
{
    public class ProductRepositoryTests
    {
        [TearDown]
        public void Cleanup()
        {
            var productsRepository = new ProductRepository();

            productsRepository.RemoveAll();
        }

        [Test]
        public void AddNullValue()
        {
            var productsRepository = new ProductRepository();

            Assert.Throws<ArgumentNullException>(() => productsRepository.Add(null));
        }

        [Test]
        public void AddInvalidEntityWithWrongId()
        {
            var productsRepository = new ProductRepository();

            Assert.Throws<ArgumentException>(() => productsRepository.Add(new Product
            {
                Id = string.Empty,
                Name = "test"
            }));
        }

        [Test]
        public void AddInvalidEntityWithWrongName()
        {
            var productsRepository = new ProductRepository();

            Assert.Throws<ArgumentException>(() => productsRepository.Add(new Product
            {
                Id = "test",
                Name = string.Empty
            }));
        }

        [Test]
        public void AddCorrectEntity()
        {
            var productsRepository = new ProductRepository();
            var newProduct = new Product
            {
                Id = "test",
                Name = "test"
            };

            productsRepository.Add(newProduct);

            Assert.IsTrue(productsRepository.GetAll().Count() == 1);
            Assert.AreEqual(productsRepository.GetAll().First().Name, newProduct.Name);
            Assert.AreEqual(productsRepository.GetAll().First().Id, newProduct.Id);
        }

        [Test]
        public void AddExistingEntityException()
        {
            var productsRepository = new ProductRepository();
            var newProduct = new Product
            {
                Id = "test",
                Name = "test"
            };

            productsRepository.Add(newProduct);
                
            Assert.Throws<ArgumentException>(() => productsRepository.Add(newProduct));
        }

        [Test]
        public void RemoveEntity()
        {
            var productsRepository = new ProductRepository();
            var newProduct = new Product
            {
                Id = "test",
                Name = "test"
            };

            productsRepository.Add(newProduct);
            productsRepository.RemoveAll();

            Assert.IsTrue(productsRepository.GetAll().Count() == 0);
        }
    }
}
