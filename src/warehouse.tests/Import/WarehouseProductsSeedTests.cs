﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using warehouse.Exceptions;
using warehouse.Import.Seeds;

namespace warehouse.tests.Import
{
    public class WarehouseProductsSeedTests
    {
        [Test]
        public void WrongInputData()
        {
            var productSeed = new WarehouseProductsSeed();

            Assert.Throws<SeedInputDataException>(() => productSeed.Seed(new[] { string.Empty }));
        }
    }
}
