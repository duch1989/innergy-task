﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using warehouse.Exceptions;
using warehouse.Import.Seeds;

namespace warehouse.tests.Import
{
    public class ProductSeedTests
    {
        [Test]
        public void WrongInputData()
        {
            var productSeed = new ProductSeed();

            Assert.Throws<SeedInputDataException>(() => productSeed.Seed(new[] { string.Empty }));
        }
    }
}
