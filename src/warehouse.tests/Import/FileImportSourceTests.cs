﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using warehouse.Import;

namespace warehouse.tests.Import
{
    public class FileImportSourceTests
    {
        [Test]
        public void FileNotExistException()
        {
            Assert.Throws<FileNotFoundException>(() => new FileImportSource("notExistingFile.txt"));
        }


        [Test]
        public void FileGetNextOk()
        {
            var fileImportSource = new FileImportSource("TestImportFile.txt");

            var result = fileImportSource.GetNext();

            Assert.AreEqual(result, "test");
        }

        [Test]
        public void DisposeAndGetNextException()
        {
            var fileImportSource = new FileImportSource("TestImportFile.txt");
            fileImportSource.Dispose();

            Assert.Throws<ObjectDisposedException>(() => fileImportSource.GetNext());
        }
    }
}
