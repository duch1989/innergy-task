﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using warehouse.Data;
using warehouse.Import;
using warehouse.Interfaces;
using warehouse.Repositories;

namespace warehouse.tests.Import
{
    public class ImportServiceTests
    {
        public const string CommentLineExample = "# Material inventory initial state as of Jan 01 2018";
        public const string DataExample = "Cherry Hardwood Arched Door - PS;COM-100001;WH-A,5|WH-B,10";
        public Product ExampleProduct = new Product() { Id = "COM-100001", Name = "Cherry Hardwood Arched Door - PS" };
        public WarehouseProduct ExampleWarehouseProduct1 = new WarehouseProduct() { WarehouseId = "WH-A", ProductId = "COM-100001", Quantity = 5 };
        public WarehouseProduct ExampleWarehouseProduct2 = new WarehouseProduct() { WarehouseId = "WH-B", ProductId = "COM-100001", Quantity = 10 };

        [TearDown]
        public void Cleanup()
        {
            var warehouseProductsRepository = new WarehouseProductsRepository();
            var productsRepository = new ProductRepository();

            productsRepository.RemoveAll();
            warehouseProductsRepository.RemoveAll();
        }

        [Test]
        public void EmptySourceFileTest()
        {
            var warehouseProductsRepository = new WarehouseProductsRepository();
            var productsRepository = new ProductRepository();
            var fileImportDataSource = new Mock<IImportDataSource>();
            var importService = new ImportService();
            fileImportDataSource.Setup(i => i.GetNext()).Returns<string>(null);

            importService.Import(fileImportDataSource.Object);

            Assert.Zero(warehouseProductsRepository.GetAll().Count());
            Assert.Zero(productsRepository.GetAll().Count());
        }

        [Test]
        public void SourceFileWithCommentLineTest()
        {
            var warehouseProductsRepository = new WarehouseProductsRepository();
            var productsRepository = new ProductRepository();
            var fileImportDataSource = new Mock<IImportDataSource>();
            var importService = new ImportService();
            fileImportDataSource.SetupSequence(i => i.GetNext())
                .Returns(CommentLineExample)
                .Returns((string?)null);

            importService.Import(fileImportDataSource.Object);

            Assert.Zero(warehouseProductsRepository.GetAll().Count());
            Assert.Zero(productsRepository.GetAll().Count());
        }

        [Test]
        public void SourceFileWithCommentLineAndDataLineTest()
        {
            var warehouseProductsRepository = new WarehouseProductsRepository();
            var productsRepository = new ProductRepository();
            var fileImportDataSource = new Mock<IImportDataSource>();
            var importService = new ImportService();
            fileImportDataSource.SetupSequence(i => i.GetNext())
                .Returns(CommentLineExample)
                .Returns(DataExample)
                .Returns((string?)null);

            importService.Import(fileImportDataSource.Object);

            var importedProducts = productsRepository.GetAll();
            var importedWarehouseProducts = warehouseProductsRepository.GetAll().ToArray();

            Assert.AreEqual(importedWarehouseProducts.Count(), 2);
            Assert.AreEqual(importedWarehouseProducts[0].WarehouseId, ExampleWarehouseProduct1.WarehouseId);
            Assert.AreEqual(importedWarehouseProducts[0].ProductId, ExampleWarehouseProduct1.ProductId);
            Assert.AreEqual(importedWarehouseProducts[0].Quantity, ExampleWarehouseProduct1.Quantity);
            Assert.AreEqual(importedWarehouseProducts[1].WarehouseId, ExampleWarehouseProduct2.WarehouseId);
            Assert.AreEqual(importedWarehouseProducts[1].ProductId, ExampleWarehouseProduct2.ProductId);
            Assert.AreEqual(importedWarehouseProducts[1].Quantity, ExampleWarehouseProduct2.Quantity);

            Assert.AreEqual(importedProducts.Count(), 1);
            Assert.AreEqual(importedProducts.First().Name, ExampleProduct.Name);
            Assert.AreEqual(importedProducts.First().Id, ExampleProduct.Id);
        }
    }
}
